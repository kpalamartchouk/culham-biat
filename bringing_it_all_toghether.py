#!/usr/bin/env python

import argparse
import logging
import sys

from collections import namedtuple

from scipy.interpolate import interp1d
from scipy.signal import medfilt

import ppfmock as ppf


PPF_DATA = namedtuple('PPF_DATA', ['data', 'x', 't', 'nd', 'nx', 'nt',
                                   'dunits', 'xunits', 'tunits',
                                   'desc', 'comm', 'seq', 'ier'])


class PPFError(Exception):
    pass


def read_args():
    """Parse command line arguments
    """

    parser = argparse.ArgumentParser(description="Extract data from the"
                                     "mock database and show some nice tricks")

    parser.add_argument('--datastore', default='./datastore',
                        help="Location of the datastore")
    parser.add_argument('--logging_level', default=logging.ERROR,
                        type=int, choices=range(logging.NOTSET,
                                                logging.FATAL + 1),
                        help="Logging level")
    parser.add_argument('--print_data', default=False, action='store_true',
                        help="Print the synopsys of the dataset")
    parser.add_argument('--median_filter', default=None, type=int,
                        help="Apply median filter with the specified "
                        "half-width before doing anything else. "
                        "Only applicable to 1-D data")
    parser.add_argument('--plot_data', default=False, action='store_true',
                        help="Plot the dataset")
    parser.add_argument('--interpolate', default=None,
                        type=lambda x: [float(n) for n in x.split(',')],
                        help="Takes a comma-delimited list of numbers and "
                        "prints interpolated values of the result for them. "
                        "Only applicable to 1-D data")
    parser.add_argument('source', type=lambda x: [n for n in x.split(',')],
                        help="Data source specification: a comma-separated "
                        "list 'shot, seq, dda, dtype, uid'")


    args = parser.parse_args()

    return args


def get_result(datastore, logger, source):
    """Extract desired data set from the database
    """

    ppf.ROOT = datastore

    shot, seq, dda, dtype, uid = source

    ppf_data = ppf.ppfdata(int(shot), int(seq), dda, dtype, uid)

    if ppf_data[-1]:
        res_err = ppf.ppferr('ppfdata', ppf_data[-1])
        raise PPFError(res_err)

    logger.info("SHOT: {shot}   DDA: {dda}   MSEQ:{mseq}".format(
        shot=shot,
        dda=dda,
        mseq=ppf.ppfmseq(shot, dda=dda, uid=uid)))

    if dda.startswith('ECM'):
        result = PPF_DATA(*(
            (ppf_data[0].reshape((ppf_data[5], ppf_data[4])),) +
            ppf_data[1:]))
    else:
        result = PPF_DATA(*ppf_data)

    return result


def interpolate(data_set, logger, arguments):
    """Demonstrating the interpolation routine from scipy
    """
    if data_set.data.ndim != 1:
        raise PPFError("I can only interpolate 1-D data")
    try:
        interpolator = interp1d(data_set.t, data_set.data)
    except ValueError as message:
        logger.critical(
            "A problem when interpolating: {}".format(message))
        sys.exit(2)
    return interpolator(arguments)


def plot_2d_data(data_set, logger):
    """Demonstrating basic 2-D plotting
    """

    import matplotlib.pyplot as plt
    plt.plot(data_set.t, data_set.data)
    plt.title(data_set.desc)
    plt.xlabel(data_set.tunits)
    plt.ylabel(data_set.dunits)
    plt.show()


def plot_3d_data(data_set, logger):
    """Demonstrating basic 3-D plotting
    """
    import matplotlib.pyplot as plt
    from matplotlib import cm
    from mpl_toolkits.mplot3d import Axes3D
    import numpy as np

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    X, Y = np.meshgrid(data_set.x, data_set.t)
    Z = data_set.data

    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm)

    ax.set_xlabel('X')
    ax.set_ylabel('t')
    ax.set_zlabel('data')

    fig.colorbar(surf)

    plt.show()


if __name__ == "__main__":

    args = read_args()

    logger = logging.getLogger('ppf_tool')
    logger.setLevel(args.logging_level)

    try:
        result = get_result(args.datastore, logger, args.source)
    except PPFError as error_message:
        logger.critical(error_message)
        sys.exit(1)

    if args.median_filter is not None:
        if result.data.ndim != 1:
            logger.error("I can only filter 1-D data")
        else:
            result = PPF_DATA(*((medfilt(result.data),) + result[1:]))

    if args.print_data:
        print(result)

    if args.plot_data:
        if result.data.ndim == 1:
            plot_2d_data(result, logger)
        else:
            plot_3d_data(result, logger)

    if args.interpolate is not None:
        print("Restuls of interpolation: {}".format(
            interpolate(result, logger, args.interpolate)))
