import numpy as np
import argparse
import os
import pickle

if 1:
    try:
        import ppf as PPF
    except:
        pass

import ppfmock as ppf

def read_args():
    args = None

    parser = argparse.ArgumentParser(description='')

    parser.add_argument('--datastore', default='./datastore'
                        , dest='datastore', action='store'
                        , help='Location of the datastore')

    parser.add_argument('--create'
                        , dest='create', action='store_true')
    
    args = parser.parse_args()

    return args


def create_datastore( root, srcs ):

    if not os.path.exists(  root ):
        os.makedirs(  root )

    for src in srcs:
        shot,seq,dda,dtype,uid = src

        dda=dda.strip()
        dtype=dtype.strip()
        uid = uid.strip()

        alldata = PPF.ppfdata( shot, dda, dtype, seq=seq, uid=uid )
        
        err = alldata[-1]

        if not err:

            data,x,t,nd,nx,nt,dunits,xunits,tunits,desc,comm,seq,ier = alldata
        
            odir = ppf.data_location( shot, seq, dda, dtype, uid )
                                            
            if not os.path.exists( odir ):
                os.makedirs( odir )

            ofile = '{0}/data.txt'.format( odir )

            with open( ofile, 'wb' ) as f:
                pickle.dump( alldata, f )
            
if __name__ == "__main__":

    args = read_args()

    ppf.ROOT = args.datastore

    srcs = [ 
        (88709, 66, 'PT5P', 'BA2L', 'JETPPF'),
        (88709, 65, 'SXR ', 'H02 ', 'JETPPF'),
        (88705, 20, 'ECM1', 'PRFL', 'JETPPF'),
        (88704, 14, 'ECM1', 'PRFL', 'JETPPF'),
        (80128, 26, 'KG1V', 'LID3', 'JETPPF'),
        (80128, 12, 'KG1V', 'LID3', 'JETPPF'),
        (88711, 17, 'KG1V', 'LID3', 'JETPPF')
        ]

    if args.create:
        print("datastore location:{0}".format(args.datastore))

        create_datastore( args.datastore, srcs )

    else:

        for src in srcs:
            shot,seq,dda,dtype,uid = src 
            data = ppf.ppfdata( shot, seq, dda, dtype )
            if data[-1]:
                res_err = ppf.ppferr( 'ppfdata', data[-1] )
                print(res_err)


    for src in srcs:
        shot = src[0]
        dda=src[2]
        print( shot, dda, ppf.ppfmseq( src[0], dda=dda, uid='JETPPF' ) )
        dda='****'
        print( shot,dda, ppf.ppfmseq( src[0], dda=dda, uid='JETPPF' ) )

        res = ppf.ppfdata( shot, 0, src[2], src[3], uid=src[4] )
        if res[-1]:
            res_err = ppf.ppferr( 'ppfdata', res[-1] )
            print( res_err )

        data,x,t,nd,nx,nt,dunits,xunits,tunits,desc,comm,seq,ier = res


    bad_srcs = [
        (88711, 16, 'KG1V', 'LID3', 'JETPPF'),
        (88711, 17, 'KG2V', 'LID3', 'JETPPF'),
        (88711, 17, 'KG1V', 'XXXX', 'JETPPF'),
        (88711, 17, 'KG1V', 'LID3', 'FRED'),
        (12354, 0, 'KG1V', 'LID3', 'JETPPF'),
        (88711, 0, 'KG2V', 'LID3', 'JETPPF'),
        (88711, 0, 'KG1V', 'XXXX', 'JETPPF'),
        (88711, 0, 'KG1V', 'LID3', 'FRED')
        ]

    print("NOW TRY BAD DATA SOURCES")
    for src in bad_srcs:

        res = ppf.ppfdata( src[0], src[1], src[2], src[3], src[4] )

        if res[-1]:
            res_err = ppf.ppferr( 'ppfdata', res[-1] )

            print(res_err, "PASS" if res[-1] == 1 else "FAIL")
            
        if res[-1] != 1:
            print("ppfdata:unexpected result for bad src" )
        
        

 
