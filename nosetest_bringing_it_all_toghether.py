#!/usr/bin/env python

import nose
import logging
from bringing_it_all_toghether import *


DATASTORE = './datastore'

LOGGER = logging.getLogger('dummy')

GOOD_SOURCES = [
    (88709, 66, 'PT5P', 'BA2L', 'JETPPF'),
    (88709, 65, 'SXR ', 'H02 ', 'JETPPF'),
    (88705, 20, 'ECM1', 'PRFL', 'JETPPF'),
    (88704, 14, 'ECM1', 'PRFL', 'JETPPF'),
    (80128, 26, 'KG1V', 'LID3', 'JETPPF'),
    (80128, 12, 'KG1V', 'LID3', 'JETPPF'),
    (88711, 17, 'KG1V', 'LID3', 'JETPPF')]

BAD_SOURCES = [
    (88711, 16, 'KG1V', 'LID3', 'JETPPF'),
    (88711, 17, 'KG2V', 'LID3', 'JETPPF'),
    (88711, 17, 'KG1V', 'XXXX', 'JETPPF'),
    (88711, 17, 'KG1V', 'LID3', 'FRED'),
    (12354, 0, 'KG1V', 'LID3', 'JETPPF'),
    (88711, 0, 'KG2V', 'LID3', 'JETPPF'),
    (88711, 0, 'KG1V', 'XXXX', 'JETPPF'),
    (88711, 0, 'KG1V', 'LID3', 'FRED')]


def test_get_results_good():
    for source in GOOD_SOURCES:
        get_result(DATASTORE, LOGGER, source)


def test_get_results_bad():
    for source in BAD_SOURCES:
        try:
            get_result(DATASTORE, LOGGER, source)
            raise RuntimeError("Got good when expecting bad result for "
                               "source {}".format(source))
        except PPFError:
            pass
