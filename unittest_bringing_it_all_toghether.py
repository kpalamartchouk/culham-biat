#!/usr/bin/env python

import unittest

import logging
from bringing_it_all_toghether import *


DATASTORE = './datastore'

LOGGER = logging.getLogger('dummy')

GOOD_SOURCES = [
    (88709, 66, 'PT5P', 'BA2L', 'JETPPF'),
    (88709, 65, 'SXR ', 'H02 ', 'JETPPF'),
    (88705, 20, 'ECM1', 'PRFL', 'JETPPF'),
    (88704, 14, 'ECM1', 'PRFL', 'JETPPF'),
    (80128, 26, 'KG1V', 'LID3', 'JETPPF'),
    (80128, 12, 'KG1V', 'LID3', 'JETPPF'),
    (88711, 17, 'KG1V', 'LID3', 'JETPPF')]

BAD_SOURCES = [
    (88711, 16, 'KG1V', 'LID3', 'JETPPF'),
    (88711, 17, 'KG2V', 'LID3', 'JETPPF'),
    (88711, 17, 'KG1V', 'XXXX', 'JETPPF'),
    (88711, 17, 'KG1V', 'LID3', 'FRED'),
    (12354, 0, 'KG1V', 'LID3', 'JETPPF'),
    (88711, 0, 'KG2V', 'LID3', 'JETPPF'),
    (88711, 0, 'KG1V', 'XXXX', 'JETPPF'),
    (88711, 0, 'KG1V', 'LID3', 'FRED')]


class TestGetResults(unittest.TestCase):

    def test_get_results_good(self):
        for source in GOOD_SOURCES:
            result = get_result(DATASTORE, LOGGER, source)
            self.assertEqual(result[-1], 0)

    def test_get_results_bad(self):
        for source in BAD_SOURCES:
            with self.assertRaises(PPFError):
                get_result(DATASTORE, LOGGER, source)


if __name__ == '__main__':
    unittest.main()
