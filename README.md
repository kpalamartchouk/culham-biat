# PPFMOCK

## Original example program

Create the local datastore and run tests

```
usage: example.py [-h] [--datastore DATASTORE] [--create]
```

optional arguments:
```   
 -h, --help            show this help message and exit

 --datastore DATASTORE

   Location of the datastore

 --create

   create the datastore
```

## Modified example program
``
usage: bringing_it_all_toghether.py [-h] [--datastore DATASTORE]
                                    [--logging_level {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50}]
                                    [--print_data]
                                    [--median_filter MEDIAN_FILTER]
                                    [--plot_data] [--interpolate INTERPOLATE]
                                    source

Extract data from themock database and show some nice tricks

positional arguments:
  source                Data source specification: a comma-separated list
                        'shot, seq, dda, dtype, uid'

optional arguments:
  -h, --help            show this help message and exit
  --datastore DATASTORE
                        Location of the datastore
  --logging_level {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50}
                        Logging level
  --print_data          Print the synopsys of the dataset
  --median_filter MEDIAN_FILTER
                        Apply median filter with the specified half-width
                        before doing anything else. Only applicable to 1-D
                        data
  --plot_data           Plot the dataset
  --interpolate INTERPOLATE
                        Takes a comma-delimited list of numbers and prints
                        interpolated values of the result for them. Only
                        applicable to 1-D data
```
