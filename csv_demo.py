#!/usr/bin/env python

import sys

import numpy as np
import matplotlib.pyplot as plt


if __name__ == "__main__":

    in_file_name = sys.argv[1]

    data = np.loadtxt(in_file_name,
                      delimiter=",")

    plt.plot(data[:, 0], data[:, 1])
    plt.show()
