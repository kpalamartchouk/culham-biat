import os
import pickle
import logging

logging.info("IMPORTING PPF MOCK")

ROOT='.'

def seq_location( shot, uid ):
    path = '{0}/{1}/{2}'.format( ROOT, shot, uid )
    return path

def data_location( shot, seq, dda, dtype, uid ):
    seq_path = seq_location( shot, uid )
    data_path = '{0}/{1}/{2}/{3}'.format( seq_path,  seq
                                       , dda, dtype )
    return data_path


def ppferr( fname, err):
    """
    Return error string for function fname and error code
    """
    s = ''
    if err:
        s = '{0}:Error {1} in function'.format( fname, err )

    return s

def ppfmseq( shot, dda='****', uid='JETPPF' ):
    """
    Returns the latest version number of the data specified by shot and dda.
    """
    err=0

    dda=dda.strip()
    uid = uid.strip()

    path = seq_location( shot, uid )
    seq_dirs = []

    try:
        seq_dirs = os.listdir(path)
    except:

        err = 1

    mseq = -1

    if seq_dirs:
        seqs=[]

        if '****' == dda:
            seqs = [ int( s ) for s in seq_dirs ]
        else:#look in dirs for dda
            for d in seq_dirs:
                ddas = os.listdir( path + '/' +d )

                for dda2 in ddas:
                    if dda2 == dda:
                        seqs.append(int(d))

        try:
            mseq = sorted( seqs )[-1]
        except:
            err = 1



    return (mseq,err)

def ppfdata( shot, seq, dda, dtype, uid='JETPPF' ):
    """
    data,x,t,nd,nx,nt,dunits,xunits,tunits,desc,comm,seq,ier = ppfdata()
    """

    if 0 == seq:
        seq,err = ppfmseq( shot, dda )

    dda=dda.strip()
    dtype=dtype.strip()
    uid = uid.strip()

    idir = data_location( shot, seq, dda, dtype, uid )
    ifile = '{0}/data.txt'.format( idir )
    #print(ifile)

    alldata = None

    try:
        with open( ifile, 'rb' ) as f:
            alldata = pickle.load( f )

            seq_ = alldata[-2]
    except:
        err = 1
        alldata = (err,)
        #print( "EXCEPTION")

    return (alldata)
